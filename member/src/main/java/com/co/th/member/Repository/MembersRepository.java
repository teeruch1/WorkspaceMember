package com.co.th.member.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.co.th.member.model.Members;


@Repository
/*public interface AccountRepository extends JpaRepository<AccountType, Long>{*/

public interface MembersRepository extends JpaRepository<Members, Long>{

	List<Members> findAll();

}
