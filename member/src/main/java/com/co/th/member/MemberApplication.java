package com.co.th.member;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.co.th.member.service.MembersService;
import com.co.th.member.utility.Mapping;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


@RestController
@SpringBootApplication
public class MemberApplication{
	protected Log logger = LogFactory.getLog(this.getClass());
    
	MembersService accountTypeService;
	
	public static void main(String[] args) {
		SpringApplication.run(MemberApplication.class, args);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = Mapping.CONTROLLER.LOGIN)
	 public ModelAndView Welcome(HttpServletRequest request,HttpSession session) { 
		ModelAndView modelAndView = new ModelAndView(Mapping.PAGE.LOGIN);
		logger.info("Start Project...");
		
		modelAndView.addObject("test","");
		return modelAndView; 
	}

}
