package com.co.th.member.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.co.th.member.model.Members;
import com.co.th.member.service.MembersService;
import com.co.th.member.utility.Mapping;
 

@Controller
@SpringBootApplication
public class AddAccountTypeCtrl{

	@Autowired
	private MembersService membersService;
	
	/*
	 * @Autowired private JdbcTemplate
	 */
	
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	 public ModelAndView Welcome(HttpServletRequest request,HttpSession session) { 
		ModelAndView modelAndView = new ModelAndView(Mapping.PAGE.LOGIN);
		List<Members> listAcc = membersService.findAll();
		return modelAndView; 
	}

}
