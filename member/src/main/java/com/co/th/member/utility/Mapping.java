package com.co.th.member.utility;

public class Mapping {
	public static final String API = "/api";
	public static final String USER = "/User";
	public static final String ADMIN = "/Admin";

	public static final String ROLE_1 = "Merchant";
	public static final String ROLE_9 = "SystemAdmin";
	
	public static final String REDIRECT = "redirect:";
	
	
	public class CONTROLLER {
		public static final String INDEX = "/index";
		public static final String LOGIN = "/login";
	}
	
    public class PAGE {
    	public static final String INDEX = "/index";
		public static final String LOGIN = "/app/view/login/Login";
		
	}
	

}
