package com.co.th.member.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.co.th.member.Repository.MembersRepository;
import com.co.th.member.model.Members;

@Service
public class MembersService{
	
	private MembersRepository membersRepository;

	public List<Members> findAll() {
		return membersRepository.findAll();
	}

}
